﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Line : Point
    {
        public float CoordinateZ { get; set; }
        public float CoordinateK { get; set; }

        private Random Rnd = new Random(DateTime.Now.Millisecond);

        public Line(int coordinateX, int coordinateY, int colorR, int colorG, int colorB, int coordinateZ, int coordinateK) : base(coordinateX, coordinateY, colorR, colorG, colorB)
        {
            this.CoordinateZ = coordinateZ;
            this.CoordinateK = coordinateK;
        }

        public Line() : base()
        {
            this.CoordinateX = Rnd.Next(495);
            this.CoordinateY = Rnd.Next(495);
            this.CoordinateZ = Rnd.Next(495);
            this.CoordinateK = Rnd.Next(495);
        }

        public Line(Line previousLine) : base (previousLine)
        {
            this.CoordinateZ = previousLine.CoordinateZ;
            this.CoordinateK = previousLine.CoordinateK;
        }

        public void CordinateChange(int coordinateX, int coordinateY, int coordinateZ, int coordinateK)
        {
            base.CordinateChange(coordinateX, coordinateY);
            this.CoordinateZ = coordinateZ;
            this.CoordinateK = coordinateK;
        }

        public override Graphics Draw(Graphics graphics)
        {
            Color randomColor = Color.FromArgb(this.ColorR, this.ColorG, this.ColorB);
            Pen pen = new Pen(randomColor);
            SolidBrush color = new SolidBrush(randomColor);
            graphics.FillEllipse(color, this.CoordinateX, this.CoordinateY, 10, 10);
            graphics.FillEllipse(color, this.CoordinateZ, this.CoordinateK, 10, 10);
            graphics.DrawLine(pen, this.CoordinateX+5, this.CoordinateY+5, this.CoordinateZ+5, this.CoordinateK+5);
            return graphics;
        }
    }

}

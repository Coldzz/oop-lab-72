﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Ellipse : Circle
    {
        public int SecondRadius { get; set; }

        private Random Rnd = new Random(DateTime.Now.Millisecond);

        public Ellipse(int coordinateX, int coordinateY, int colorR, int colorG, int colorB, int radius, int secondRadius) : base(coordinateX, coordinateY, colorR, colorG, colorB, radius)
        {
            this.SecondRadius = secondRadius;
        }

        public Ellipse() : base()
        {
            this.CoordinateX = Rnd.Next(399);
            this.CoordinateY = Rnd.Next(399);
            this.SecondRadius = Rnd.Next(100);
        }

        public Ellipse(Ellipse previousEllipse) : base(previousEllipse)
        {
            this.SecondRadius = previousEllipse.SecondRadius;
        }

        public override Graphics Draw(Graphics graphics)
        {
            Color randomColor = Color.FromArgb(this.ColorR, this.ColorG, this.ColorB);
            Pen pen = new Pen(randomColor);
            SolidBrush color = new SolidBrush(randomColor);
            graphics.FillEllipse(color, this.CoordinateX + this.Radius / 2, this.CoordinateY + this.SecondRadius / 2, 10, 10);
            graphics.DrawEllipse(pen, this.CoordinateX+5, this.CoordinateY+5, Radius, SecondRadius);
            graphics.DrawLine(pen, this.CoordinateX + 5 + Radius, this.CoordinateY + 5 + SecondRadius / 2, this.CoordinateX + 5 + Radius / 2, this.CoordinateY + 5 + SecondRadius / 2);
            return graphics;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Point : Shape
    {
        public float CoordinateX { get; set; }
        public float CoordinateY { get; set; }
        public int ColorR { get; set; }
        public int ColorG { get; set; }
        public int ColorB { get; set; }

        private Random Rnd = new Random(DateTime.Now.Millisecond);

        public Point(int coordinateX, int coordinateY, int colorR, int colorG, int colorB)
        {
            this.CoordinateX = coordinateX;
            this.CoordinateY = coordinateY;
            this.ColorR = colorR;
            this.ColorG = colorG;
            this.ColorB = colorB;
        }

        public Point()
        {
            this.CoordinateX = Rnd.Next(495);
            this.CoordinateY = Rnd.Next(495);
            this.ColorR = Rnd.Next(255);
            this.ColorG = Rnd.Next(255);
            this.ColorB = Rnd.Next(255);
        }

        public Point(Point previousShapePoint)
        {
            this.CoordinateX = previousShapePoint.CoordinateX;
            this.CoordinateY = previousShapePoint.CoordinateY;
            this.ColorR = previousShapePoint.ColorR;
            this.ColorG = previousShapePoint.ColorG;
            this.ColorB = previousShapePoint.ColorB;
        }

        public virtual void CordinateChange(int coordinateX, int coordinateY)
        {
            this.CoordinateX = coordinateX;
            this.CoordinateY = coordinateY;
        }

        public void ColorChange(int colorR, int colorG, int colorB)
        {
            this.ColorR = colorR;
            this.ColorG = colorG;
            this.ColorB = colorB;
        }

        public override Graphics Draw(Graphics graphics)
        {
            Color randomColor = Color.FromArgb(this.ColorR, this.ColorG, this.ColorB);
            SolidBrush color = new SolidBrush(randomColor);
            graphics.FillEllipse(color, this.CoordinateX, this.CoordinateY, 10, 10);
            return graphics;
        }
    }
}

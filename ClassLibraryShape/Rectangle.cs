﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Rectangle : Line
    {
        private Random Rnd = new Random(DateTime.Now.Millisecond);
        public Rectangle() : base()
        {
            this.CoordinateX = Rnd.Next(250);
            this.CoordinateY = Rnd.Next(250);
            this.CoordinateZ = Rnd.Next(250);
            this.CoordinateK = Rnd.Next(250);
        }
        public override Graphics Draw(Graphics graphics)
        {
            Color randomColor = Color.FromArgb(this.ColorR, this.ColorG, this.ColorB);
            Pen pen = new Pen(randomColor);
            SolidBrush color = new SolidBrush(randomColor);
            graphics.FillEllipse(color, this.CoordinateX, this.CoordinateY, 10, 10);
            graphics.FillEllipse(color, this.CoordinateX + this.CoordinateZ, this.CoordinateY + this.CoordinateK, 10, 10);
            graphics.DrawRectangle(pen, this.CoordinateX + 5, this.CoordinateY + 5, this.CoordinateZ, this.CoordinateK);
            return graphics;
        }
    }
}

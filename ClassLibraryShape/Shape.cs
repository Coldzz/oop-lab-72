﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Shape
    {
        public virtual Graphics Draw(Graphics graphics)
        {
            return graphics;
        }
        public static Graphics Hex(Graphics graphics)
        {
            Random Rnd = new Random(DateTime.Now.Millisecond);
            Shape Result = null;
            for (int i = 0; i <= 20; i++)
            {
                int s = Rnd.Next(1, 6);
                switch (s)
                {
                    case 1:
                        Result = new Point();
                        break;
                    case 2:
                        Result = new Line();
                        break;
                    case 3:
                        Result = new Circle();
                        break;
                    case 4:
                        Result = new Rectangle();
                        break;
                    case 5:
                        Result = new Ellipse();
                        break;
                }
                Result.Draw(graphics);
            }
            return graphics;
        }
    }
}

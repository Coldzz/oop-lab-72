﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibraryShape
{
    public class Circle : Point
    {
        public int Radius { get; set; }

        private Random Rnd = new Random(DateTime.Now.Millisecond);

        public Circle(int coordinateX, int coordinateY, int colorR, int colorG, int colorB, int radius) : base(coordinateX, coordinateY, colorR, colorG, colorB)
        {
            this.Radius = radius;
        }

        public Circle() : base()
        {
            this.CoordinateX = Rnd.Next(435);
            this.CoordinateY = Rnd.Next(435);
            this.Radius = Rnd.Next(50);
        }

        public Circle(Circle previousCircle) : base(previousCircle)
        {
            this.Radius = previousCircle.Radius;
        }

        public override Graphics Draw(Graphics graphics)
        {
            Color randomColor = Color.FromArgb(this.ColorR, this.ColorG, this.ColorB);
            Pen pen = new Pen(randomColor);
            SolidBrush color = new SolidBrush(randomColor);
            graphics.FillEllipse(color, this.CoordinateX + this.Radius/2, this.CoordinateY + this.Radius/2, 10, 10);
            graphics.DrawEllipse(pen, this.CoordinateX+5, this.CoordinateY+5, this.Radius, this.Radius);
            graphics.DrawLine(pen, this.CoordinateX + 5 + Radius, this.CoordinateY + 5 + Radius/2, this.CoordinateX + 5 + Radius / 2, this.CoordinateY + 5 + Radius / 2);
            return graphics;
        }
    }
}
